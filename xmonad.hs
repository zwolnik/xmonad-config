import XMonad
import XMonad.Util.EZConfig
-- drop output of a func
import XMonad.Util.Ungrab
-- EMWH compatibility
import XMonad.Hooks.EwmhDesktops
-- for xmobar
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import XMonad.Layout.IndependentScreens
-- for fullscreen
import XMonad.Layout.NoBorders
import XMonad.Hooks.ManageHelpers
-- gaps
import XMonad.Layout.Spacing
-- shells
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Prompt.Pass
import XMonad.Prompt.Man

main :: IO ()
main = do
  screens <- countScreens
  xmobarProcs <- mapM (\n -> spawnPipe $ "xmobar -x " ++ show n) [0..screens-1]
  xmonad
    $ ewmhFullscreen
    $ ewmh
    $ docks
    $ myConfig xmobarProcs


startup :: X ()
startup = do
    spawn "feh --bg-fill /$HOME/.config/xmonad/background.jpg"
    spawn "picom -b"
    spawn "dunst &"
    spawn "blueberry-tray"
    spawn "flameshot"

myConfig xmobarProcs = def
    { modMask = mod4Mask
    , terminal = "alacritty"
    , borderWidth = 2
    , normalBorderColor = "#313131"
    , focusedBorderColor = "#4abaff"
    -- , worspaces = [ "ᚡ", "ᚢ", "ᚣ", "ᚤ", "ᚥ", "ᚦ", "ᚧ", "ᚨ", "ᚩ" ]
    , startupHook = startup
    , layoutHook = avoidStruts $ smartBorders myLayoutHook
    , logHook = myLogHook xmobarProcs
    , manageHook = manageDocks <+> myManageHook
    }
  `additionalKeysP`
    [ ("M-S-l",   spawn "slock")
    , ("<Print>", unGrab *> spawn "flameshot gui")
    , ("M-x",     shellPrompt promptCfg)
    , ("M-s",     passPrompt promptCfg)
    , ("M-S-s",   passTypePrompt promptCfg)
    , ("M-C-s",   passGeneratePrompt promptCfg)
    , ("M-S-m",   manPrompt promptCfg)
    , ("M-q",     spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else dunstify 'XMonad not it path' \"PATH: $PATH\"; fi")
    , ("<XF86AudioRaiseVolume>",  unGrab *> spawn "volumectl up")
    , ("<XF86AudioLowerVolume>",  unGrab *> spawn "volumectl down")
    , ("<XF86AudioMute>",         unGrab *> spawn "volumectl toggle")
    , ("<XF86MonBrightnessUp>",   unGrab *> spawn "backlightctl +")
    , ("<XF86MonBrightnessDown>", unGrab *> spawn "backlightctl -")
    ]

myLayoutHook = tiled ||| tiled' ||| Full
  where
     tiled   = spacingWithEdge 5 $ Tall nmaster delta ratio
     tiled'  = spacingWithEdge 5 $ Tall nmaster delta ratio'
     nmaster = 1
     ratio   = 1/2
     ratio'  = 2/3
     delta   = 3/100

myLogHook xmobarProcs = mapM_ (\handle -> dynamicLogWithPP $ xmobarPP
    { ppOutput = hPutStrLn handle
    , ppCurrent = xmobarColor "#c792ea" "" . wrap "<box type=Bottom width=2 mb=2 color=#c792ea>" "</box>"
    , ppVisible = xmobarColor "#c792ea" ""
    , ppHidden = xmobarColor "#1190f7" "" . wrap "<box type=Bottom width=2 mb=2 color=#1190f7>" "</box>"
    , ppHiddenNoWindows = xmobarColor "#1190f7" ""
    , ppTitle = xmobarColor "#b3afc2" "" . shorten 60
    , ppSep =  "<fc=#666666><fn=1> | </fn></fc>"
    , ppUrgent = xmobarColor "#c45500" "" . wrap "!" "!"
    }) xmobarProcs

myManageHook = composeAll
    [ isFullscreen --> doFullFloat
    ]

promptCfg :: XPConfig
promptCfg = def
    { font = "xft:FiraCode Nerd Font:size 10"
    , bgColor = "#313131"
    , bgHLight = "#1190f7"
    , borderColor = "#1190f7"
    , promptBorderWidth = 1
    , height = 25
    , autoComplete = Just 100000
    , showCompletionOnTab = False
    , position = Top
    }
